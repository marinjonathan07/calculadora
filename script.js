const num1 = document.getElementById('num1');
const num2 = document.getElementById('num2');
const metodo = document.getElementById('metodo');
const submit = document.querySelector('button[type="submit"]');

document.getElementById('formulario').addEventListener('submit', (event) => {
  event.preventDefault();
  calcular();
});

function calcular() {
  let resultado;
  const operacion = parseInt(metodo.value);

  switch (operacion) {
    case 1:
      resultado = parseFloat(num1.value) + parseFloat(num2.value);
      break;
    case 2:
      resultado = parseFloat(num1.value) - parseFloat(num2.value);
      break;
    case 3:
      resultado = parseFloat(num1.value) * parseFloat(num2.value);
      break;
    case 4:
      resultado = parseFloat(num1.value) / parseFloat(num2.value);
      break;
    case 5:
      resultado = parseFloat(num1.value) % parseFloat(num2.value);
      break;
    default:
      resultado = 'Error: operación no válida';
      break;
  }

  document.getElementById('resultados').textContent = `El resultado es: ${resultado}`;
}
